<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.0.1/css/ol.css" type="text/css">
    <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <style>
        .map {
            height: 850px;
            width: 100%;
        }

        #map{
            position:absolute;
            z-index:1;
            width:100%; height:100%;
            top:0; bottom:0;
        }
        .ol-popup {
            position: absolute;
            background-color: white;
            -webkit-filter: drop-shadow(0 1px 4px rgba(0,0,0,0.2));
            filter: drop-shadow(0 1px 4px rgba(0,0,0,0.2));
            padding: 15px;
            border-radius: 10px;
            border: 1px solid #cccccc;
            bottom: 12px;
            left: -50px;
            width: 230px;
        }
        .ol-popup:after, .ol-popup:before {
            top: 100%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }
        .ol-popup:after {
            border-top-color: white;
            border-width: 10px;
            left: 48px;
            margin-left: -10px;
        }
        .ol-popup:before {
            border-top-color: #cccccc;
            border-width: 11px;
            left: 48px;
            margin-left: -11px;
        }
        .ol-popup-content {
            position: relative;
            min-width: 200px;
            min-height: 150px;
            height: 100%;
            max-height: 250px;
            padding:2px;
            white-space: normal;
            background-color: #f7f7f9;
            border: 1px solid #e1e1e8;
            overflow-y: auto;
            overflow-x: hidden;
        }
        .ol-popup-content p{
            font-size: 14px;
            padding: 2px 4px;
            color: #222;
            margin-bottom: 15px;
        }
        .ol-popup-closer {
            position: absolute;
            top: -4px;
            right: 2px;
            font-size: 100%;
            color: #0088cc;
            text-decoration: none;
        }
        a.ol-popup-closer:hover{
            color: #005580;
            text-decoration: underline;
        }
        .ol-popup-closer:after {
            content: "✖";
        }
    </style>
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList"></script>
    <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.0.1/build/ol.js"></script>
    <title>OpenLayers example</title>
</head>
<body>
<h2>My Map</h2>
<div id="map" class="map">
    <div id="popup" class="ol-popup">
        <a href="#" id="popup-closer" class="ol-popup-closer"></a>
        <div id="popup-content"></div>
    </div>
</div>
<script type="text/javascript">


    var iconFeatures=[];

    var iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([78.0766, 10.9601], 'EPSG:4326',
            'EPSG:3857')),
        name: 'driver1',
        mobile: 1234568790,
        cab: 'RedTaxi'
    });

    var iconFeature1 = new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([78.020919, 10.985205], 'EPSG:4326',
            'EPSG:3857')),
        name: 'driver2',
        mobile: 9876543218,
        cab: 'Mini'

    });

    iconFeatures.push(iconFeature);
    iconFeatures.push(iconFeature1);

    var vectorSource = new ol.source.Vector({
        features: iconFeatures //add an array of features
    });

    var iconStyle = new ol.style.Style({
        image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            opacity: 0.75,
            src: '/assets/images/redtaxi_map_pin/red_taxi/car-gray.png',
            scale: 1
        }))
    });


    var vectorLayer = new ol.layer.Vector({
        source: vectorSource,
        style: iconStyle
    });

    var map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }), vectorLayer
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([78.0766, 10.9601]), // karur lat lon
            zoom: 10
        })
    });

    // var element = document.getElementById('popup');
    //
    // var popup = new ol.Overlay({
    //     element: element,
    //     positioning: 'bottom-center',
    //     stopEvent: false,
    //     offset: [0, -50]
    // });
    // map.addOverlay(popup);
    //
    // // display popup on click
    // map.on('click', function(event) {
    //     var feature = map.forEachFeatureAtPixel(event.pixel,
    //         function(feature) {
    //             return feature;
    //         });
    //     if (feature) {
    //         var coordinates = feature.getGeometry().getCoordinates();
    //         popup.setPosition(coordinates);
    //         $(element).popover({
    //             'placement': 'top',
    //             'html': true,
    //             'name': feature.get('name') + ' mobile: ' + feature.get('mobile') + ' cab: ' + feature.get('cab')
    //         });
    //         $(element).popover('show');
    //     } else {
    //         $(element).popover('destroy');
    //     }
    // });

    var container = document.getElementById('popup');
    var content = document.getElementById('popup-content');
    var closer = document.getElementById('popup-closer');

    var overlay = new ol.Overlay({
        element: container,
        autoPan: true,
        autoPanAnimation: {
            duration: 250
        }
    });
    map.addOverlay(overlay);

    closer.onclick = function() {
        overlay.setPosition(undefined);
        closer.blur();
        return false;
    };

    function cabDetailsPopup(event)
    {
        if (map.hasFeatureAtPixel(event.pixel) === true) {
            var coordinate = event.coordinate;
            var feature = map.forEachFeatureAtPixel(event.pixel,
                function(feature) {
                    return feature;
                });
            content.innerHTML = '<b>Name : </b>' + feature.get('name') +
                '<br><b>Mobile Number : </b>'+feature.get('mobile') +
                '<br><b>Cab : </b>'+feature.get('cab');
            overlay.setPosition(coordinate);
        }
    }

    map.on('singleclick', function (event) {
        overlay.setPosition(undefined);
        closer.blur();
    });

    // Mouse cursor change
    map.on('pointermove', function(event) {
        map.getTargetElement().style.cursor =
            map.hasFeatureAtPixel(event.pixel) ? 'pointer' : '';
        cabDetailsPopup(event);
    });

</script>
</body>
</html>
